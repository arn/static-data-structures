#include "static_containers.h"

#include <chrono>
#include <gtest/gtest.h>
#include <random>
#include <unordered_map>
#include <vector>

class UnorderedMapPerfTest : public ::testing::Test {
public:
  template <size_t map_size, size_t value_size> void unordered_map_perf() {
    struct PretendBigClass {
      size_t data[value_size];

      PretendBigClass(size_t i) { data[0] = i; }

      PretendBigClass(){};

      bool operator==(const PretendBigClass& other) const {
        return other.data[0] == data[0];
      }
    };

    std::vector<std::string> strings;

    for (size_t i = 0; i < map_size; i++) {
      strings.push_back(random_string(32));
    }

    ASSERT_EQ(strings.size(), map_size);

    std::unordered_map<std::string, PretendBigClass> std_map;
    ds::unordered_map<std::string, PretendBigClass, map_size> static_map;

    // Insert performance
    auto std_ins_start = std::chrono::system_clock::now();
    {
      size_t count = 0;
      for (const auto& s : strings) {
        std_map[s] = PretendBigClass(count);
        count += 1;
      }
    }
    auto std_ins_duration = std::chrono::system_clock::now() - std_ins_start;

    auto static_ins_start = std::chrono::system_clock::now();
    {
      size_t count = 0;
      for (const auto& s : strings) {
        static_map[s] = PretendBigClass(count);
        count += 1;
      }
    }
    auto static_ins_duration =
        std::chrono::system_clock::now() - static_ins_start;

    std::cout << "STD insert duration: " << std_ins_duration.count()
              << std::endl;
    std::cout << "Static insert duration: " << static_ins_duration.count()
              << std::endl;

    // Check correctness
    size_t std_visited = 0;
    for (const auto& std_pair : std_map) {
      std_visited += 1;
      ASSERT_EQ(std_pair.second, static_map[std_pair.first]);
    }
    size_t static_visited = 0;
    for (const auto& static_pair : static_map) {
      static_visited += 1;
      ASSERT_EQ(static_pair.second, std_map[static_pair.first]);
    }

    ASSERT_EQ(std_visited, static_visited);

    // Compare lookup
    auto std_lookup_start = std::chrono::system_clock::now();
    {
      size_t count = 0;
      for (const auto& s : strings) {
        ASSERT_EQ(std_map[s].data[0], count);
        count += 1;
      }
    }
    auto std_lookup_duration =
        std::chrono::system_clock::now() - std_lookup_start;

    auto static_lookup_start = std::chrono::system_clock::now();
    {
      size_t count = 0;
      for (const auto& s : strings) {
        ASSERT_EQ(static_map[s].data[0], count);
        count += 1;
      }
    }
    auto static_lookup_duration =
        std::chrono::system_clock::now() - static_lookup_start;

    std::cout << "STD lookup duration: " << std_lookup_duration.count()
              << std::endl;
    std::cout << "Static lookup duration: " << static_lookup_duration.count()
              << std::endl;
  }

  static std::string random_string(size_t len) {
    static const std::string chars(
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

    static std::random_device rd;
    static std::mt19937 generator(rd());
    static std::uniform_int_distribution<> distrib(0, sizeof(chars) - 1);

    std::string rstring;

    for (size_t i = 0; i < len; i++) {
      rstring += chars[distrib(generator)];
    }

    return rstring;
  }
};

TEST_F(UnorderedMapPerfTest, Insert) {
  unordered_map_perf<1000, 128>();
  unordered_map_perf<100000, 1>();
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
