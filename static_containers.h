#pragma once

#include "allocators/bump_allocator.h"
#include "allocators/free_list_allocator.h"

#include <limits>
#include <memory>

#if defined(DEBUG)
#include <iostream>
#define DS_DEBUG_ASSERT(x)                                             \
  if (!(x)) {                                                          \
    std::cerr << "Assertion failed at " << __FILE__ << ":" << __LINE__ \
              << std::endl;                                            \
  }
#else
#define DS_DEBUG_ASSERT(x)
#endif

#define DS_STATIC_ASSERT_IS_ALLOCATOR()                              \
  static_assert(std::is_base_of<IStaticAllocator, Allocator>::value, \
                "Allocator must be a subclass of IStaticAllocator")

#define DS_LIKELY(x) __builtin_expect(!!(x), 1)
#define DS_UNLIKELY(x) __builtin_expect((x), 0)

namespace ds {
template <class T, size_t Capacity, class Allocator = FreeListAllocator>
class list {
  struct ListNode {
    T data;
    ListNode* next;
    ListNode* previous;
  };

public:
  list() { DS_STATIC_ASSERT_IS_ALLOCATOR(); }

  size_t size() const { return size_; }

  void push_back(const T& data) {
    DS_DEBUG_ASSERT(tail_ ? tail_->next == nullptr : true);

    ListNode* new_node = allocator_.allocate();
    if (DS_UNLIKELY(new_node == nullptr)) {
      throw std::bad_alloc();
    }

    if (tail_) {
      tail_->next = new_node;
    }
    new_node->previous = tail_;
    new_node->next = nullptr;
    tail_ = new_node;

    tail_->data = data;

    if (DS_UNLIKELY(head_ == nullptr)) {
      head_ = tail_;
    }

    DS_DEBUG_ASSERT(head_ && tail_);

    size_ += 1;
  }

  void push_front(const T& data) {
    DS_DEBUG_ASSERT(head_ ? head_->previous == nullptr : true);

    ListNode* new_node = allocator_.allocate();
    if (DS_UNLIKELY(new_node == nullptr)) {
      throw std::bad_alloc();
    }

    if (DS_LIKELY(head_)) {
      head_->previous = new_node;
    }

    new_node->previous = head_;
    new_node->previous = nullptr;
    head_ = new_node;

    head_->data = data;

    if (DS_UNLIKELY(tail_ == nullptr)) {
      tail_ = head_;
    }

    size_ += 1;
  }

  void pop_back() {
    // Popping an empty STL container is undefined behavior. Reflecting this, we
    // elect to ignore such a case.
    if (DS_LIKELY(tail_)) {
      ListNode* old_tail = tail_;
      tail_ = old_tail->previous;
      allocator_.free(old_tail);

      size_ -= 1;

      if (DS_UNLIKELY(size_ == 0)) {
        head_ = nullptr;
      }
    }

    DS_DEBUG_ASSERT(size_ == 0 ? !(head_ || tail_) : true);
  }

  void pop_front() {
    // Popping an empty STL container is undefined behavior. Reflecting this, we
    // elect to ignore such a case.
    if (DS_LIKELY(head_)) {
      ListNode* old_head = head_;
      head_ = old_head->next;
      allocator_.free(old_head);

      size_ -= 1;

      if (DS_UNLIKELY(size_ == 0)) {
        tail_ = nullptr;
      }
    }

    DS_DEBUG_ASSERT(size_ == 0 ? !(head_ || tail_) : true);
  }

  /* ----- iterator ----- */
  class iterator {
  public:
    iterator(ListNode* node)
        : node(node) {}

    T& operator*() { return node->data; }

    const T& operator*() const { return node->data; }

    iterator& operator++() {
      node = node->next;
      return *this;
    }

    bool operator==(const iterator& other) { return other.node == node; }

    bool operator!=(const iterator& other) { return other.node != node; }

  private:
    ListNode* node;
  };

  iterator begin() { return iterator{head_}; }

  iterator end() { return iterator{nullptr}; }

private:
  typename Allocator::template Instance<ListNode, Capacity> allocator_;

  ListNode* head_ = nullptr;

  ListNode* tail_ = nullptr;

  size_t size_ = 0;
};

template <class K, class V, size_t Capacity, class Hash = std::hash<K>,
          class Pred = std::equal_to<K>, class Allocator = FreeListAllocator>
class unordered_map {
  using KVPair = std::pair<K, V>;

  struct Entry {
    KVPair* data = nullptr;
    Entry* next = nullptr;
  };

public:
  unordered_map() { DS_STATIC_ASSERT_IS_ALLOCATOR(); }

  V& operator[](const K& key) {
    size_t bucket_index = Hash{}(key) % Capacity;

    for (Entry* entry = hash_buckets_[bucket_index]; entry != nullptr;
         entry = entry->next) {
      if (Pred{}(entry->data->first, key)) {
        // Match
        return entry->data->second;
      }
    }

    Entry* new_entry = bucket_entry_allocator_.allocate();
    if (DS_UNLIKELY(new_entry == nullptr)) {
      throw std::bad_alloc();
    }
    KVPair* new_kv_pair = pair_allocator_.allocate();
    if (DS_UNLIKELY(new_kv_pair == nullptr)) {
      throw std::bad_alloc();
    }

    new_kv_pair->first = key;

    new_entry->data = new_kv_pair;
    new_entry->next = hash_buckets_[bucket_index];

    hash_buckets_[bucket_index] = new_entry;
    size_ += 1;

    return new_entry->data->second;
  }

  /* ----- iterator ----- */
  class iterator {
  public:
    iterator(Entry* entry, size_t bucket_index,
             const std::array<Entry*, Capacity>& hash_buckets)
        : entry_{entry}
        , bucket_index_{bucket_index}
        , hash_buckets_{hash_buckets} {}

    KVPair& operator*() { return *entry_->data; }

    const KVPair& operator*() const { return *entry_->data; }

    iterator& operator++() {
      // This will segfault if we are at end(), but that is undefined so we
      // want to catch such a case.
      if (DS_UNLIKELY(entry_->next != nullptr)) {
        entry_ = entry_->next;
      } else if (bucket_index_ < Capacity - 1) {
        do {
          bucket_index_ += 1;
        } while (hash_buckets_[bucket_index_] == nullptr &&
                 bucket_index_ < Capacity - 1);
        entry_ = hash_buckets_[bucket_index_];
      } else {
        entry_ = nullptr;
      }

      DS_DEBUG_ASSERT(bucket_index_ < Capacity);
      DS_DEBUG_ASSERT(!entry_ ? bucket_index_ == Capacity - 1 : true);
      return *this;
    }

    bool operator==(const iterator& other) { return other.entry_ == entry_; }

    bool operator!=(const iterator& other) { return other.entry_ != entry_; }

  private:
    Entry* entry_;
    size_t bucket_index_;
    const std::array<Entry*, Capacity>& hash_buckets_;
  };

  iterator begin() const {
    for (size_t bucket_index = 0; bucket_index < Capacity; bucket_index++) {
      if (hash_buckets_[bucket_index] != nullptr) {
        return iterator(hash_buckets_[bucket_index], bucket_index,
                        hash_buckets_);
      }
    }
    return iterator(nullptr, 0, hash_buckets_);
  }

  iterator end() const { return iterator(nullptr, 0, hash_buckets_); }

private:
  std::array<Entry*, Capacity> hash_buckets_{};

  typename Allocator::template Instance<KVPair, Capacity> pair_allocator_;

  typename Allocator::template Instance<Entry, Capacity>
      bucket_entry_allocator_;

  size_t size_ = 0;
};

} // namespace ds

#undef DS_STATIC_ASSERT_IS_ALLOCATOR
#undef DS_DEBUG_ASSERT