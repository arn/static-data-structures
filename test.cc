#include "static_containers.h"

#include <gtest/gtest.h>

constexpr size_t SMALL_TEST_SIZE = 100;

class ListTest : public ::testing::Test {};

TEST_F(ListTest, AllocateToCapacityNoCorruption) {
  ds::list<size_t, SMALL_TEST_SIZE> list;

  ASSERT_EQ(list.size(), 0);

  for (size_t i = 0; i < SMALL_TEST_SIZE; i++) {
    list.push_back(i);
    ASSERT_EQ(list.size(), i + 1);
  }

  {
    auto it = list.begin();
    for (size_t i = 0; i < SMALL_TEST_SIZE; i++) {
      ASSERT_EQ(i, *it);
      ++it;
    }
  }

  // Check that this syntax also works
  size_t count = 0;
  for (const auto& x : list) {
    ASSERT_EQ(x, count);
    count += 1;
  }

  try {
    list.push_back(0);
    ASSERT_TRUE(false);
  } catch (const std::bad_alloc&) {
  }

  ASSERT_EQ(list.size(), SMALL_TEST_SIZE);

  list = {};

  for (size_t i = 0; i < SMALL_TEST_SIZE; i++) {
    list.push_front(i);
  }
}

class UnorderedMapTest : public ::testing::Test {};

TEST_F(UnorderedMapTest, OperatorBracket) {
  ds::unordered_map<std::string, size_t, SMALL_TEST_SIZE> map;

  const std::string test_string_1 = "test string";
  const std::string test_string_2 = "another test string";

  map[test_string_1] = 5;
  map[test_string_2] = 10;

  ASSERT_EQ(map[test_string_1], 5);
  ASSERT_EQ(map[test_string_2], 10);
}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}