CFLAGS = -std=c++11 -g -Wall -Wextra -Wpedantic
LDFLAGS = -lgtest

all: test.cc
	clang++ test.cc -O2 -o test $(CFLAGS) $(LDFLAGS)

debug: test.cc
	clang++ test.cc -O0 -o test -DDEBUG $(CFLAGS) $(LDFLAGS)

perf: perf_test.cc
	clang++ perf_test.cc -O2 -o perf_test $(CFLAGS) $(LDFLAGS)

perf_debug: perf_test.cc
	clang++ perf_test.cc -O0 -o perf_test -DDEBUG $(CFLAGS) $(LDFLAGS)