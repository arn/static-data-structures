#pragma once
#include "allocator.h"

#include <array>
#include <bitset>

/// The simplest allocator. Should probably only ever be used for testing.
///
/// Pros: low memory overhead
/// Cons: O(n) worst-case allocations
class BumpAllocator : public IStaticAllocator {
public:
  BumpAllocator() {}

  template <class C, size_t Capacity>
  class Instance : public IStaticAllocator::Instance<C, Capacity> {
  public:
    C* allocate() override {
      for (size_t i = next_; i < Capacity; i++) {
        if (!used_[i]) {
          next_ += 1;
          next_ %= Capacity;
          used_[i] = true;
          return ::new (&bytes_[i * sizeof(C)]) C;
        }
      }
      for (size_t i = 0; i < next_; i++) {
        if (!used_[i]) {
          next_ += 1;
          next_ %= Capacity;
          used_[i] = true;
          return ::new (&bytes_[i * sizeof(C)]) C;
        }
      }
      return nullptr;
    }

    void free(C* ptr) override {
      size_t index = (ptr - reinterpret_cast<C*>(&bytes_)) / sizeof(C);
      if (index < Capacity) {
        used_[index] = false;
      }
      ::delete (ptr);
    }

  private:
    std::array<uint8_t, sizeof(C) * Capacity> bytes_;

    std::bitset<Capacity> used_;

    size_t next_;
  };
};
