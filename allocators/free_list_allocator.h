#pragma once
#include "allocator.h"

#include <array>
#include <bitset>

/// Allocates buckets of memory in O(1), but doesn't optimize for cache.
/// The best allocator that can reasonably be made without significant effort.
class FreeListAllocator : public IStaticAllocator {
public:
  FreeListAllocator() {}

  template <class C, size_t Capacity>
  class Instance : public IStaticAllocator::Instance<C, Capacity> {

    /// To enforce the ability to store a pointer without corruption.
    static constexpr size_t unit_size =
        sizeof(C) < sizeof(void*) ? sizeof(void*) : sizeof(C);

  public:
    Instance() {
      // Initialize free list
      for (size_t i = 0; i < Capacity; i++) {
        void** entry_as_raw_ptr =
            reinterpret_cast<void**>(&bytes_[i * unit_size]);
        if (i != Capacity - 1) {
          *entry_as_raw_ptr =
              reinterpret_cast<void*>(&bytes_[(i + 1) * unit_size]);
        } else {
          *entry_as_raw_ptr = nullptr;
        }
      }

      head_ = reinterpret_cast<void*>(&bytes_[0]);
    }

    C* allocate() override {
      if (!head_) {
        return nullptr;
      }

      void* allocation = head_;
      head_ = *reinterpret_cast<void**>(head_);
      return new (allocation) C;
    }

    void free(C* ptr) override {
      ::delete (ptr);
      void** ptr_as_raw = reinterpret_cast<void**>(ptr);
      *ptr_as_raw = head_;
      head_ = reinterpret_cast<void*>(ptr_as_raw);
    }

  private:
    std::array<uint8_t, unit_size * Capacity> bytes_;
    void* head_;
  };
};
