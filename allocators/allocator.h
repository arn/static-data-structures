#pragma once
#include <cstddef>

class IAllocator {
public:
  IAllocator() {}

  template <class C> class Instance {
  public:
    virtual C* allocate() = 0;
    virtual void free(C*) = 0;
  };
};

class IStaticAllocator : public IAllocator {
public:
  IStaticAllocator(){};
  IStaticAllocator(const IStaticAllocator&) = delete;
  IStaticAllocator(IStaticAllocator&&) = delete;

  template <class C, size_t Capacity>
  class Instance : public IAllocator::Instance<C> {};
};
