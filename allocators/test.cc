#include <gtest/gtest.h>

#include "allocator.h"
#include "bump_allocator.h"

constexpr size_t TEST_CAPACITY = 1000;

class AllocatorTest : public ::testing::Test {
public:
  BumpAllocator::Instance<size_t, TEST_CAPACITY> allocator;
};

TEST_F(AllocatorTest, allocate_capacity) {
  for (size_t i = 0; i < TEST_CAPACITY; i++) {
    ASSERT_NE(allocator.allocate(), nullptr);
  }
  ASSERT_EQ(allocator.allocate(), nullptr);
}

/// Add tests here

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}